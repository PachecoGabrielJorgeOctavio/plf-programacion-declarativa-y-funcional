# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)


```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor black
  .titulo {
    BackgroundColor #1E90FF
  }
  .s1 {
    BackgroundColor #00BFFF
  }
  .s2 {
    BackgroundColor #87CEFA
  }
}
</style>
* Programación Declarativa <<titulo>>
**_ surge para
*** Solucionar problemas <<s2>>
****_ de la
***** Programación Imperativa <<s1>>
******_ basada en
******* El modelo de Von Neumann <<s2>>
********_ que requiere
********* Especificaciones detalladas <<s2>>
**********_ de los
*********** Recursos utilizados <<s2>>
 
**_ relega
*** Tareas rutinarias <<s2>>
****_ al 
***** Compilador <<s2>>

**_ permite que los 
*** Programas <<s1>>
****_ sean
***** Mas cortos <<s2>>
***** Mas fáciles de realizar <<s2>>
***** Mas fáciles de depurar <<s2>>
***** Menos complejos <<s2>>
***** Menos propensos a fallar <<s2>>
***** Escritos en un código más ordenado <<s2>>

**_ recurre a 
*** Programación Funcional <<s1>>
****_ usan 
***** Lenguaje Matemático <<s2>> 
***** Razonamiento ecuacional <<s2>> 
***** Funciones de orden superior <<s2>>

*** Evaluación perezosa  <<s1>>
****_ consiste en 
***** Realizar solo las operaciones necesarias <<s2>>
******_ para
******* Cálculos posteriores <<s2>>

****_ permite
***** Definir tipos de datos infinitos <<s2>>

**_ utiliza también
*** Programación Lógica <<s1>>
****_ utiliza
***** La lógica de predicados de primer orden <<s2>>
******_ que son
******* Relaciones entre objetos definidos <<s2>>

***** Axiomas <<s2>>
***** Reglas de inferencia <<s2>> 
***** Demostración automática de teoremas <<s2>>

****_ necesita un
***** Conjunto de relaciones y predicados <<s2>>
******_ para 
******* Operar mediante algoritmos de resolución <<s2>>
********_ y así
********* Validar los predicados del sistema <<s2>>

****_ no tiene
***** Distinción clara entre argumentos y resultados <<s2>>


@endmindmap
```
[Referencia 1](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)
[Referencia 2](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)
[Referencia 3](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)

# Lenguaje de Programación Funcional
## Nuevas tendencias en las tecnologías de la información

```plantuml
@startmindmap
* Programación funcional
**_ es un 
*** Paradigma de programación
****_ es
***** Modelo de computación
******_ en el que 
******* Los lenguajes de programación
********_ brindan la
********* Semántica a un programa 

********_ estaban
********* Basados en el Modelo de Von Neumann
**********_ en el cual 
*********** Los programas 
************_ debían estar
************* Almacenadas en la misma maquina 
**************_ antes de llegar a la 
*************** Ejecución
************_ consiste en
************* Serie de instrucciones \n ejecutadas secuencialmente

************_ acceden a 
************* Una zona de memoria
**************_ mediante  
*************** Variables
****************_ que cambian su valor mediante
***************** Instrucciones de asignación

**********_ denominados 
*********** Programación imperativa
************_ de cual deriva la
************* Programación orientada a objetos
**************_ que consiste en
*************** Fragmentos de código (objetos)
****************_ que
***************** Interactúan entre si
**_ derivado del
*** Paradigma de programación declarativa 
****_ del cual varia también 
***** Paradigma de programación lógico 
******_ basado en
******* Lógica simbólica 
********_ en el cual 
********* Un programa 
**********_ es formado por
*********** Un conjunto de sentencias lógicas
************_ que definen
************* Los hechos conocidos respecto a un programa

******_ aplica 
******* Inferencia lógica
********_ para
********* Resolver Problemas
**********_ sin necesidad de
*********** Establecer la secuencia de pasos para resolverlo

**_ se basa em
*** Lambda Calculo
****_ desarrollado por 
***** Church y Kleene en 1930

****_ es un
***** Sistema Computacional
******_ de 
******* Funciones
******* Aplicaciones de funciones
******* Recursividad
****_ de la cual varia
***** Lógica combinatoria 

**_ trata a 
*** Los programas 
****_ como
***** Funciones matemáticas
******_ por lo cual
******* Desaparece el concepto de asignación de memoria
********_ teniendo que implementar
********* Funciones recursivas
**********_ las cuales
*********** Hacen referencia a sí mismas

******_ que se evalúan mediante
******* Evaluación perezosa
******* Evaluación impaciente
******* Evaluación estricta
******* Evaluación no estricta
********_ similar a la
********* Evaluación McCarthy

******_ que pueden ser
******* Parámetros de otra función
********_ denominada
******** Función de orden superior
******* Aplicaciones parciales

*** Variables
****_ solo para
***** Referirse a parámetros de entrada y salida

*** Constantes
****_ como
***** Funciones que devuelven el mismo resultado

**_ tiene 
*** Ventajas
**** Transparencia referencial
*****_ define que 
****** Los resultados de las operaciones 
*******_ son 
******** Independientes del orden en el que se hacen las operaciones 
**** Evita problemas
*****_ cuando se 
****** Modifica el estado del computo
**** Permite el paralelismo 

@endmindmap
```
[Referencia ](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)